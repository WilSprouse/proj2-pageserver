from flask import Flask
from flask import render_template, request
import os.path

app = Flask(__name__)

@app.route("/<path:url>")
def main(url):
    if (url[-4:] == "html") or (url[-3:] == "css"):
        if (os.path.exists("./templates/"+url)): 
            return render_template(url) 
        elif (('~' in url) or ('..' in url) or ('//' in url)):
            return render_template("403.html")
        else:
            return render_template("404.html")
    else:
        return render_template("404.html")

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port="5600")
